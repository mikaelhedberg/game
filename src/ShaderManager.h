//
// Created by Mikael on 6/19/2016.
//

#ifndef GAME_SHADERMANAGER_H
#define GAME_SHADERMANAGER_H

#include <GL/glew.h>
#include <string>
#include <vector>

namespace Engine {

class ShaderManager {
 public:
  static GLuint CreateProgram(const std::string& vertex_file_path, const std::string& fragment_file_path);
  static void Destroy();

 private:
  static std::string ReadShader(const std::string& file_path);
  static GLuint CreateShader(GLenum shader_type, const std::string& source);
  static std::vector<GLuint> programs_;

};

}


#endif //GAME_SHADERMANAGER_H
