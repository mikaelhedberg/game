//
// Created by Mikael on 6/19/2016.
//

#ifndef GAME_CONTEXTINFO_H
#define GAME_CONTEXTINFO_H

namespace Engine {

struct ContextInfo {
  int major_version;
  int minor_version;
  bool use_core;
};

}


#endif //GAME_CONTEXTINFO_H
