//
// Created by Mikael on 6/20/2016.
//

#ifndef GAME_MATERIAL_H
#define GAME_MATERIAL_H

#include <GL/glew.h>

namespace Engine {

class Material {
 public:
  Material(GLuint program_id);

  GLuint GetProgram() const;

 private:
  GLuint program_id_;
};

}


#endif //GAME_MATERIAL_H
