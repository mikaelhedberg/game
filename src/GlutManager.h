//
// Created by Mikael on 6/19/2016.
//

#ifndef GAME_GLUTMANAGER_H
#define GAME_GLUTMANAGER_H

#include "FrameBufferInfo.h"
#include "WindowInfo.h"
#include "ContextInfo.h"

namespace Engine {

class Updater;

class GlutManager {
 public:
  static void Initialize(WindowInfo window_info,
                         ContextInfo context_info,
                         FrameBufferInfo frame_buffer_info,
                         Updater& updater);
  static void Close();
  static void Run();
  static void PrintOpenGLInfo();

  static float GetLatestFrameSeconds();
  static WindowInfo GetWindowInfo();
  static ContextInfo GetContextInfo();
  static FrameBufferInfo GetFrameBufferInfo();

 private:
  static void IdleCallback();
  static void CloseCallback();
  static void DisplayCallback();
  static void ReshapeCallback(int width, int height);
  static void KeyPressedCallback(unsigned char key, int x, int y);
  static void MouseMoveCallback(int x, int y);
  static void MouseActiveMoveCallback(int x, int y);

};

}

#endif //GAME_GLUTMANAGER_H
