//
// Created by Mikael on 6/19/2016.
//

#ifndef GAME_GAMEOBJECT_H
#define GAME_GAMEOBJECT_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>
#include "Material.h"

namespace Engine {

//TODO: Remove the shader fromt this object since it shouldn't owe it
class GameObject {

 private:
  glm::mat4 transform_;
  GLuint vertex_array_;
  Material material_;
  std::vector<GLuint> vertex_buffers_; //This is just lazt atm, we don't need an vector for this
  int vertex_count_;

 public:
  GameObject(GLuint vertex_array,
             Material material,
             glm::mat4 transform,
             const std::vector<GLuint> &vertex_buffers,
             int vertex_count);
  void Destroy();

  //This function will handle the actual draw call.
  //Observe that you should not bind any programs here since this will be handled by the scene
  void Draw();

  void SetTransform(glm::mat4 transform);
  glm::mat4 GetTransform() const;

  const Material &GetMaterial() const;
  GLuint GetVertexArray() const;
  const std::vector<GLuint> &GetVertexBuffers() const;

};

}


#endif //GAME_GAMEOBJECT_H
