//
// Created by Mikael on 6/23/2016.
//

#include "PhysicsManager.h"

namespace Engine {

PhysicsManager::PhysicsManager() {
  collision_configuration_ = std::unique_ptr<btDefaultCollisionConfiguration>(new btDefaultCollisionConfiguration());
  collision_dispatcher_ =
      std::unique_ptr<btCollisionDispatcher>(new btCollisionDispatcher(collision_configuration_.get()));
  broadphase_ = std::unique_ptr<btDbvtBroadphase>(new btDbvtBroadphase());
  solver_ = std::unique_ptr<btSequentialImpulseConstraintSolver>(new btSequentialImpulseConstraintSolver());
  world_ = std::unique_ptr<btDiscreteDynamicsWorld>(new btDiscreteDynamicsWorld(collision_dispatcher_.get(),
                                                                                broadphase_.get(),
                                                                                solver_.get(),
                                                                                collision_configuration_.get()));
  world_->setGravity(btVector3(0, -10.0f, 0));

  auto ground_shape = new btBoxShape(btVector3(btScalar(100.0f), btScalar(50.0f), btScalar(100.0f)));

  //This will clean up the memory on exit
  collision_shapes_.push_back(ground_shape);

  btTransform ground_transform_;
  ground_transform_.setIdentity();
  ground_transform_.setOrigin(btVector3(0, -100.0f, 0));

  float mass = 0.0f;
  btVector3 local_inertia(0, 0, 0);

  auto ground_motion_state = new btDefaultMotionState(ground_transform_);
  btRigidBody::btRigidBodyConstructionInfo rb_info(mass, ground_motion_state, ground_shape, local_inertia);
  auto ground_rigid_body = new btRigidBody(rb_info);

  //I assume that this will clean up the memory on exit as well
  world_->addRigidBody(ground_rigid_body);
}

void PhysicsManager::SetGravity(float gravity) {
  gravity_ = gravity;
  world_->setGravity(btVector3(0, gravity_, 0));
}

float PhysicsManager::GetGravity() const {
  return gravity_;
}

void PhysicsManager::SetGroundPosition(const btVector3& position) {

}

btVector3 PhysicsManager::GetGroundPosition() const {
  return btVector3();
}

void PhysicsManager::AttachObject(const btVector3& box_dimensions, const btTransform& start_transform) {
  auto shape = new btBoxShape(box_dimensions);
  collision_shapes_.push_back(shape);
  float mass = 1.0f;
  btVector3 local_inertia(0.0f, 0.0f, 0.0f);
  shape->calculateLocalInertia(mass, local_inertia);

  btDefaultMotionState *motion_state = new btDefaultMotionState(start_transform);
  btRigidBody::btRigidBodyConstructionInfo rb_info(mass, motion_state, shape, local_inertia);
  btRigidBody *body = new btRigidBody(rb_info);

  world_->addRigidBody(body);
}

btTransform PhysicsManager::GetTransform(int insertion_index) {
  btCollisionObject *object = world_->getCollisionObjectArray()[insertion_index + 1]; // since the ground is always 1
  btRigidBody *body = btRigidBody::upcast(object);
  btTransform result;
  if (body && body->getMotionState()) {
    body->getMotionState()->getWorldTransform(result);

  } else {
    result = object->getWorldTransform();
  }

  return result;
}

void PhysicsManager::Update(float time_step) {
  world_->stepSimulation(time_step, 10);
}

}

