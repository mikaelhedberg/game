//
// Created by Mikael on 6/19/2016.
//

#include "Camera.h"
#include "InputManager.h"
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

namespace Engine {

Camera::Camera() {

  // Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
  projection_ = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f);

  //TODO: Review the updating of the camera
  // Camera matrix
  transformation_ = glm::lookAt(
      glm::vec3(0, 0, -7), // Camera is at (4,3,-3), in World Space
      glm::vec3(0, 0, 0), // and looks at the origin
      glm::vec3(0, 1, 0));  // Head is up (set to 0,-1,0 to look upside-down)

  yaw_ = -120;
  pitch_ = 0;
  eye_vector_ = glm::vec3(10, 10, 10);

  projection_view_ = projection_ * transformation_;
}

Camera::Camera(glm::mat4 projection, glm::mat4 transformation) :
    projection_(projection), transformation_(transformation) { }

void Camera::SetProjection(glm::mat4 projection) {
  projection_ = projection;
  projection_view_ = projection_ * transformation_;
}

void Camera::SetTransformation(glm::mat4 transformation) {
  transformation_ = transformation;
  projection_view_ = projection_ * transformation_;
}

glm::mat4 Camera::GetProjection() const {
  return projection_;
}

glm::mat4 Camera::GetTransformation() const {
  return transformation_;
}

glm::mat4 Camera::GetProjectionView() const {
  return projection_view_;
}

void Camera::Update() {

  const float speed = 0.5f;
  const float yaw_sensitivity = 0.01f;
  const float pitch_sensitivity = 0.01f;

  yaw_ += yaw_sensitivity * InputManager::GetDeltaX();
  pitch_ += pitch_sensitivity * InputManager::GetDeltaY();

  //roll can be removed from here. because is not actually used in FPS camera
  glm::mat4 pitch_matrix = glm::mat4(1.0f);//identity matrix
  glm::mat4 yaw_matrix = glm::mat4(1.0f);//identity matrix

  pitch_matrix = glm::rotate(pitch_matrix, pitch_, glm::vec3(1.0f, 0.0f, 0.0f));
  yaw_matrix = glm::rotate(yaw_matrix, yaw_, glm::vec3(0.0f, 1.0f, 0.0f));

  //order matters
  glm::mat4 rotate = pitch_matrix * yaw_matrix;

  //row major
  glm::vec3 forward(transformation_[0][2], transformation_[1][2], transformation_[2][2]);
  glm::vec3 strafe(transformation_[0][0], transformation_[1][0], transformation_[2][0]);

  float dx = InputManager::MoveLeft() ? -1.0f : (InputManager::MoveRight() ? 1.0f : 0.0f);
  float dz = InputManager::MoveBackward() ? -1.0f : (InputManager::MoveForward() ? 1.0f : 0.0f);

  //forward vector must be negative to look forward.
  eye_vector_ += (-dz * forward + dx * strafe) * speed;

  glm::mat4 translate = glm::mat4(1.0f);
  translate = glm::translate(translate, -eye_vector_);

  transformation_ = rotate * translate;
  projection_view_ = projection_ * transformation_;
}

}

