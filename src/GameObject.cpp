//
// Created by Mikael on 6/19/2016.
//

#include "GameObject.h"

namespace Engine {

GameObject::GameObject(GLuint vertex_array,
                       Material material,
                       glm::mat4 transform,
                       const std::vector<GLuint> &vertex_buffers,
                       int vertex_count) :
    vertex_array_(vertex_array),
    material_(material),
    transform_(transform),
    vertex_buffers_(vertex_buffers),
    vertex_count_(vertex_count) { }

void GameObject::Destroy() {
  glDeleteVertexArrays(1, &vertex_array_);
  glDeleteBuffers(vertex_buffers_.size(), &vertex_buffers_[0]);
  vertex_buffers_.clear();
}

void GameObject::SetTransform(glm::mat4 transform) {
  transform_ = transform;
}

glm::mat4 GameObject::GetTransform() const {
  return transform_;
}

const Material &GameObject::GetMaterial() const {
  return material_;
}

GLuint GameObject::GetVertexArray() const {
  return vertex_array_;
}

const std::vector<GLuint> &GameObject::GetVertexBuffers() const {
  return vertex_buffers_;
}

void GameObject::Draw() {
  glBindVertexArray(vertex_array_);
  glDrawArrays(GL_TRIANGLES, 0, vertex_count_);
}

}

