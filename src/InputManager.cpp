//
// Created by Mikael on 6/20/2016.
//

#include "InputManager.h"

namespace Engine {

bool InputManager::move_left_ = false;
bool InputManager::move_right_ = false;
bool InputManager::move_forwards_ = false;
bool InputManager::move_backward = false;
int InputManager::delta_x_ = 0;
int InputManager::delta_y_ = 0;

void InputManager::PressKey(unsigned char key) {
  switch (key) {
    case 'w': {
      move_forwards_ = true;
      break;
    }
    case 's': {
      move_backward = true;
      break;
    }
    case 'a': {
      move_left_ = true;
      break;
    }
    case 'd': {
      move_right_ = true;
      break;
    }
    default:
      break;
  }
}

void InputManager::MoveMouse(int x, int y) {
  static int previous_x = 0;
  static int previous_y = 0;

  delta_x_ = x - previous_x;
  delta_y_ = y - previous_y;

  previous_x = x;
  previous_y = y;
}

bool InputManager::MoveLeft() {
  return move_left_;
}

bool InputManager::MoveRight() {
  return move_right_;
}

bool InputManager::MoveForward() {
  return move_forwards_;
}

bool InputManager::MoveBackward() {
  return move_backward;
}

int InputManager::GetDeltaX() {
  return delta_x_;
}

int InputManager::GetDeltaY() {
  return delta_y_;
}

void InputManager::ReleaseKey(unsigned char key) {
  switch (key) {
    case 'w': {
      move_forwards_ = false;
      break;
    }
    case 's': {
      move_backward = false;
      break;
    }
    case 'a': {
      move_left_ = false;
      break;
    }
    case 'd': {
      move_right_ = false;
      break;
    }
    default:
      break;
  }
}

void InputManager::InvalidateMove() {
  delta_x_ = 0;
  delta_y_ = 0;
}

}

