//
// Created by Mikael on 6/19/2016.
//

#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>

#include "Game.h"
#include "GlutManager.h"
#include "ShaderManager.h"
#include "GameObjectFactory.h"

namespace Engine {

Game::Game() { }

void Game::Update() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearColor(0.3, 0.3, 0.3, 0.3);
  scene_.Update();
  scene_.Draw();
  std::cout << "Frame time: " << GlutManager::GetLatestFrameSeconds() << std::endl;
}

void Game::Start() {
  InitializeGlut();
  InitializeScene();
  GlutManager::Run();
}

void Game::Reshape(int width, int height) {

}

void Game::Exit() {
  scene_.Destroy();
  ShaderManager::Destroy();
}

void Game::InitializeGlut() {
  WindowInfo window;
  window.name = "Explosion game";
  window.position_x = 400;
  window.position_y = 200;
  window.width = 1024;
  window.height = 768;
  window.can_reshape = true;

  ContextInfo context;
  context.major_version = 4;
  context.minor_version = 5;
  context.use_core = true;

  FrameBufferInfo frame_buffer;
  frame_buffer.flags = GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE;
  GlutManager::Initialize(window, context, frame_buffer, *this);
}

void Game::InitializeScene() {
  GameObjectFactory factory;
  auto program = ShaderManager::CreateProgram("shaders/basicVertex.glsl", "shaders/basicFragment.glsl");

  //Let us create a cube of size N
  float height_origin = 10.0f;
  int N = 5;
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      for (int k = 0; k < N; k++) {
        glm::mat4 transform(1.0f);
        transform = glm::translate(transform, glm::vec3(i * 2.0f, height_origin + k * 2.0f, j * 2.0f));
        scene_.Attach(factory.CreateBox(program, transform));
      }
}

}

