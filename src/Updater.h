//
// Created by Mikael on 6/19/2016.
//

#ifndef GAME_UPDATER_H
#define GAME_UPDATER_H

namespace Engine {

class Updater {
 public:
  virtual void Update() = 0;
  virtual void Reshape(int width, int height) = 0;
  virtual void Exit() = 0;
};
}

#endif //GAME_UPDATER_H
