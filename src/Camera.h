//
// Created by Mikael on 6/19/2016.
//

#ifndef GAME_CAMERA_H
#define GAME_CAMERA_H

#include <glm/glm.hpp>

namespace Engine {

class Camera {
 public:
  Camera();
  Camera(glm::mat4 projection, glm::mat4 transformation);

  void Update();
  void SetProjection(glm::mat4 projection);
  void SetTransformation(glm::mat4 transformation);

  glm::mat4 GetProjection() const;
  glm::mat4 GetTransformation() const;
  glm::mat4 GetProjectionView() const;

 private:
  glm::mat4 projection_;
  glm::mat4 transformation_;
  glm::mat4 projection_view_;

  glm::vec3 eye_vector_;
  float yaw_;
  float pitch_;

};

}


#endif //GAME_CAMERA_H
