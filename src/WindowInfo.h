//
// Created by Mikael on 6/19/2016.
//

#ifndef GAME_WINDOWINFO_H
#define GAME_WINDOWINFO_H

#include <string>

namespace Engine {

struct WindowInfo {
  std::string name;
  int position_x;
  int position_y;
  bool can_reshape;
  int width;
  int height;
};

}


#endif //GAME_WINDOWINFO_H
