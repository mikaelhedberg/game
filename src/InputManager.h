//
// Created by Mikael on 6/20/2016.
//

#ifndef GAME_INPUTMANAGER_H
#define GAME_INPUTMANAGER_H


namespace Engine {

class InputManager {

 public:
  static void PressKey(unsigned char key);
  static void ReleaseKey(unsigned char key);
  static void MoveMouse(int x, int y);
  static void InvalidateMove();

  static bool MoveLeft();
  static bool MoveRight();
  static bool MoveForward();
  static bool MoveBackward();

  static int GetDeltaX();
  static int GetDeltaY();

 private:
  static bool move_left_;
  static bool move_right_;
  static bool move_forwards_;
  static bool move_backward;

  static int delta_x_;
  static int delta_y_;

};

}


#endif //GAME_INPUTMANAGER_H
