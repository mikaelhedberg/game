//
// Created by Mikael on 6/19/2016.
//

#ifndef GAME_GAME_H
#define GAME_GAME_H

#include "Updater.h"
#include "Scene.h"

namespace Engine {

class Game: public Updater {

 private:
  Scene scene_;

 public:
  Game();
  void Start();

  void Update() override;
  void Reshape(int width, int height) override;
  void Exit() override;

 private:
  void InitializeGlut();
  void InitializeScene();
};

}


#endif //GAME_GAME_H
