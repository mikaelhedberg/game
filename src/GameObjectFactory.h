//
// Created by Mikael on 6/19/2016.
//

#ifndef GAME_GAMEOBJECTFACTORY_H
#define GAME_GAMEOBJECTFACTORY_H

#include <glm/glm.hpp>
#include "GameObject.h"

namespace Engine {

class GameObjectFactory {

 public:
  GameObject CreateBox(GLuint program, glm::mat4 transform = glm::mat4(1.0f));

};

}


#endif //GAME_GAMEOBJECTFACTORY_H
