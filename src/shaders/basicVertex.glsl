#version 450 core

layout(location = 0) in vec3 vertex_position_model;
layout(location = 1) in vec3 vertex_color;

out vec3 fragment_color;
uniform mat4 mvp_transform;

void main(){
	gl_Position =  mvp_transform * vec4(vertex_position_model, 1);
	fragment_color = vertex_color;
}