#include "Game.h"

using namespace Engine;

int main() {
  Game game;
  game.Start();
  return 0;
}