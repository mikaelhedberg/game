//
// Created by Mikael on 6/23/2016.
//

#ifndef GAME_PHYSICSMANAGER_H
#define GAME_PHYSICSMANAGER_H

#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>
#include <vector>
#include <memory>

namespace Engine {

class PhysicsManager {

 public:
  PhysicsManager();

  void Update(float time_step);

  void SetGravity(float gravity);
  float GetGravity() const;

  void SetGroundPosition(const btVector3& position);
  btVector3 GetGroundPosition() const;

  void AttachObject(const btVector3& box_dimensions, const btTransform& start_transform);
  btTransform GetTransform(int insertion_index);

 private:
  float gravity_;
  btAlignedObjectArray<btCollisionShape*> collision_shapes_;

  std::unique_ptr<btDefaultCollisionConfiguration> collision_configuration_;
  std::unique_ptr<btCollisionDispatcher> collision_dispatcher_;
  std::unique_ptr<btBroadphaseInterface> broadphase_;
  std::unique_ptr<btSequentialImpulseConstraintSolver> solver_;
  std::unique_ptr<btDiscreteDynamicsWorld> world_;

};

}


#endif //GAME_PHYSICSMANAGER_H
