//
// Created by Mikael on 6/19/2016.
//

#ifndef GAME_SCENE_H
#define GAME_SCENE_H

#include <GL/glew.h>
#include "GameObject.h"
#include "Camera.h"
#include "PhysicsManager.h"
#include <vector>

namespace Engine {

class Scene {

 private:
  std::vector<GameObject> objects_;
  Camera camera_;
  GLuint current_program_;
  GLuint mvp_id_;
  PhysicsManager physics_manager_;

 public:
  Scene();
  void Draw();
  void Destroy();
  void Attach(GameObject object);
  void Update();
};

}


#endif //GAME_SCENE_H
