//
// Created by Mikael on 6/19/2016.
//

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <assert.h>
#include <iostream>
#include "GlutManager.h"
#include "Updater.h"
#include "InputManager.h"

namespace Engine {

static WindowInfo window_info_;
static ContextInfo context_info_;
static FrameBufferInfo frame_buffer_info_;
static float frame_seconds_ = 0;
static Updater *updater_ = nullptr;
static int mouse_x_ = 0;
static int mouse_y_ = 0;
static bool mouse_moved_ = false;

static bool w_down_ = false;
static bool s_down_ = false;
static bool d_down_ = false;
static bool a_down_ = false;

void GlutManager::Initialize(WindowInfo window_info,
                             ContextInfo context_info,
                             FrameBufferInfo frame_buffer_info,
                             Updater &updater) {

  window_info_ = window_info;
  context_info_ = context_info;
  frame_buffer_info_ = frame_buffer_info;
  updater_ = &updater;

  int dummy_argc = 1;
  char *dummy_argv[] = {nullptr};
  glutInit(&dummy_argc, dummy_argv);

  if (context_info_.use_core) {
    glutInitContextVersion(context_info_.major_version, context_info_.minor_version);
    glutInitContextProfile(GLUT_CORE_PROFILE);
  }
  else {
    glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
  }

  glutInitDisplayMode(frame_buffer_info_.flags);
  glutInitWindowPosition(window_info_.position_x, window_info_.position_y);
  glutInitWindowSize(window_info_.width, window_info_.height);

  glutCreateWindow(window_info_.name.c_str());

  glutIdleFunc(IdleCallback);
  glutCloseFunc(CloseCallback);
  glutDisplayFunc(DisplayCallback);
  glutReshapeFunc(ReshapeCallback);
  glutKeyboardFunc(KeyPressedCallback);
  glutPassiveMotionFunc(MouseMoveCallback);
  glutMotionFunc(MouseActiveMoveCallback);

  glewExperimental = true;
  auto result = glewInit();
  assert(result == GLEW_OK);

  //cleanup
  glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

  PrintOpenGLInfo();

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glCullFace(GL_BACK);
}

WindowInfo GlutManager::GetWindowInfo() {
  return window_info_;
}

ContextInfo GlutManager::GetContextInfo() {
  return context_info_;
}

FrameBufferInfo GlutManager::GetFrameBufferInfo() {
  return frame_buffer_info_;
}

float GlutManager::GetLatestFrameSeconds() {
  return frame_seconds_;
}


void GlutManager::Run() {
  glutMainLoop();
}

void GlutManager::Close() {
  glutLeaveMainLoop();
}

void GlutManager::IdleCallback() {
  glutPostRedisplay();
}

void GlutManager::DisplayCallback() {

  static float old_time = 0;
  float current_time = static_cast<float>(glutGet(GLUT_ELAPSED_TIME) / 1000.0);
  frame_seconds_ = current_time - old_time;
  old_time = current_time;

  assert(updater_);
  updater_->Update();
  glutSwapBuffers();
  InputManager::MoveMouse(mouse_x_, mouse_y_);
  if (!mouse_moved_)
    InputManager::InvalidateMove();
  mouse_moved_ = false;

  if (w_down_) {
    InputManager::PressKey('w');
    w_down_ = false;
  }
  else
    InputManager::ReleaseKey('w');

  if (d_down_) {
    InputManager::PressKey('d');
    d_down_ = false;
  }
  else
    InputManager::ReleaseKey('d');

  if (s_down_) {
    InputManager::PressKey('s');
    s_down_ = false;
  }
  else {
    InputManager::ReleaseKey('s');
  }

  if (a_down_) {
    InputManager::PressKey('a');
    a_down_ = false;
  }
  else {
    InputManager::ReleaseKey('a');
  }
}

void GlutManager::KeyPressedCallback(unsigned char key, int x, int y) {
  switch (key) {
    case 'w': {
      w_down_ = true;
      break;
    }
    case 's': {
      s_down_ = true;
      break;
    }
    case 'a': {
      a_down_ = true;
      break;
    }
    case 'd': {
      d_down_ = true;
      break;
    }
  }
}

void GlutManager::MouseActiveMoveCallback(int x, int y) {
  mouse_moved_ = true;
  mouse_x_ = x;
  mouse_y_ = y;
}


void GlutManager::MouseMoveCallback(int x, int y) {
  mouse_x_ = x;
  mouse_y_ = y;
}

void GlutManager::ReshapeCallback(int width, int height) {
  assert(updater_);
  updater_->Reshape(width, height);
}

void GlutManager::CloseCallback() {
  Close();
}
void GlutManager::PrintOpenGLInfo() {

  const unsigned char *renderer = glGetString(GL_RENDERER);
  const unsigned char *vendor = glGetString(GL_VENDOR);
  const unsigned char *version = glGetString(GL_VERSION);

  std::cout << "*******************************************************************************" << std::endl;

  std::cout << "GLUT:\tVendor : " << vendor << std::endl;
  std::cout << "GLUT:\tRenderer : " << renderer << std::endl;
  std::cout << "GLUT:\tOpenGl version: " << version << std::endl;
  std::cout << "GLUT:\tInitial window is '" << window_info_.name << "', with dimensions (" << window_info_.width
      << "X" << window_info_.height;
  std::cout << ") starts at (" << window_info_.position_x << "X" << window_info_.position_y;
  std::cout << ") and " << ((window_info_.can_reshape) ? "is" : "is not ") << " redimensionable" << std::endl;
  std::cout << "GLUT:\tInitial Framebuffer contains double buffers for" << std::endl;

  std::cout << "GLUT:\t OpenGL context is " << context_info_.major_version << "." << context_info_.minor_version;
  std::cout << " and profile is " << ((context_info_.use_core) ? "core" : "compatibility") << std::endl;

  std::cout << "*****************************************************************" << std::endl;
}

}







