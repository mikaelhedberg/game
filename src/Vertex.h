//
// Created by Mikael on 6/19/2016.
//

#ifndef GAME_VERTEX_H
#define GAME_VERTEX_H

#include <glm/glm.hpp>

namespace Engine {

struct Vertex {
  glm::vec3 position;
  glm::vec3 color;

  Vertex(const glm::vec3 &position, const glm::vec3 &color) {
    this->position = position;
    this->color = color;
  }

  Vertex() { }
};

}

#endif //GAME_VERTEX_H
