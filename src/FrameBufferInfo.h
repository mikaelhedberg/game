//
// Created by Mikael on 6/19/2016.
//

#ifndef GAME_FRAMEBUFFERINFO_H
#define GAME_FRAMEBUFFERINFO_H

namespace Engine {

struct FrameBufferInfo {
  unsigned int flags;
};

}


#endif //GAME_FRAMEBUFFERINFO_H
