//
// Created by Mikael on 6/20/2016.
//

#include "Material.h"

namespace Engine {

Material::Material(GLuint
                   program_id) :
    program_id_(program_id) {

}
GLuint Material::GetProgram() const {
  return program_id_;
}

}