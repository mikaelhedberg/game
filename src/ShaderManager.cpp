//
// Created by Mikael on 6/19/2016.
//

#include <assert.h>
#include <fstream>
#include <iostream>
#include <GL/glew.h>

#include "ShaderManager.h"

namespace Engine {

std::vector<GLuint> ShaderManager::programs_;

GLuint ShaderManager::CreateProgram(const std::string &vertex_file_path, const std::string &fragment_file_path) {

  std::string vertex_shader_code = ReadShader(vertex_file_path);
  std::string fragment_shader_code = ReadShader(fragment_file_path);

  GLuint vertex_shader = CreateShader(GL_VERTEX_SHADER, vertex_shader_code);
  GLuint fragment_shader = CreateShader(GL_FRAGMENT_SHADER, fragment_shader_code);

  int link_result = 0;

  GLuint program = glCreateProgram();
  glAttachShader(program, vertex_shader);
  glAttachShader(program, fragment_shader);

  glLinkProgram(program);
  glGetProgramiv(program, GL_LINK_STATUS, &link_result);

  if (link_result == GL_FALSE) {

    int info_log_length = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_log_length);
    std::string program_log;
    program_log.reserve(static_cast<unsigned int>(info_log_length));
    glGetProgramInfoLog(program, info_log_length, NULL, &program_log[0]);
    std::cout << "Shader Loader : LINK ERROR" << std::endl << &program_log[0] << std::endl;
    assert(false);
  }

  programs_.push_back(program);
  return program;
}

std::string ShaderManager::ReadShader(const std::string &file_path) {
  std::string shaderCode;
  std::ifstream file(file_path, std::ios::in);

  assert(file.good());

  file.seekg(0, std::ios::end);
  shaderCode.resize(static_cast<unsigned int>(file.tellg()));
  file.seekg(0, std::ios::beg);
  file.read(&shaderCode[0], shaderCode.size());
  file.close();

  return shaderCode;
}

GLuint ShaderManager::CreateShader(GLenum shader_type, const std::string &source) {
  int compile_result = 0;

  GLuint shader = glCreateShader(shader_type);
  const char *shader_code_ptr = source.c_str();
  const int shader_code_size = source.size();

  glShaderSource(shader, 1, &shader_code_ptr, &shader_code_size);
  glCompileShader(shader);
  glGetShaderiv(shader, GL_COMPILE_STATUS, &compile_result);

  if (compile_result == GL_FALSE) {

    int info_log_length = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);
    std::string shader_log;
    shader_log.reserve(static_cast<unsigned int>(info_log_length));
    glGetShaderInfoLog(shader, info_log_length, NULL, &shader_log[0]);
    std::cout << "ERROR compiling shader: " << std::endl << &shader_log[0] << std::endl;
    assert(false);
  }
  return shader;

}
void ShaderManager::Destroy() {
  for (auto program : programs_)
    glDeleteProgram(program);
}

}





