//
// Created by Mikael on 6/19/2016.
//

#include "Scene.h"
#include "GlutManager.h"

namespace Engine {

Scene::Scene() { }

void Scene::Update() {
  camera_.Update();
  physics_manager_.Update(GlutManager::GetLatestFrameSeconds());

  //Ok, now we need to set all the transformations of the game objects
  //so that they correspond to the physics simulation
  for (size_t i = 0; i < objects_.size(); i++) {
    auto& game_object = objects_[i];
    btTransform physics_transform = physics_manager_.GetTransform(i);

    //TODO: move this into another function
    auto rotation = physics_transform.getBasis();
    auto translation = physics_transform.getOrigin();

    glm::mat4 object_transform(1.0f);
    btVector3 x_basis = rotation.getColumn(0);
    btVector3 y_basis = rotation.getColumn(1);
    btVector3 z_basis = rotation.getColumn(2);
    object_transform[0][0] = x_basis.x();
    object_transform[0][1] = x_basis.y();
    object_transform[0][2] = x_basis.z();
    object_transform[1][0] = y_basis.x();
    object_transform[1][1] = y_basis.y();
    object_transform[1][2] = y_basis.z();
    object_transform[2][0] = z_basis.x();
    object_transform[2][1] = z_basis.y();
    object_transform[2][2] = z_basis.z();
    object_transform[3][0] = translation.x();
    object_transform[3][1] = translation.y();
    object_transform[3][2] = translation.z();

    game_object.SetTransform(object_transform);
  }
}

void Scene::Draw() {
  for (auto &object : objects_) {
    auto program = object.GetMaterial().GetProgram();

    //TODO: sort the objects so that we don't need to call the shader on every object
    if (current_program_ != program) {
      glUseProgram(program);
      current_program_ = program;
    }

    glm::mat4 mvp_transformation = camera_.GetProjectionView() * object.GetTransform();
    glUniformMatrix4fv(mvp_id_, 1, GL_FALSE, &mvp_transformation[0][0]);

    object.Draw();
  }
}

void Scene::Destroy() {
  for (auto &object : objects_) {
    object.Destroy();
  }
}

void Scene::Attach(GameObject object) {
  objects_.push_back(object);
  auto transform = object.GetTransform();

  //TODO: place this in another function
  btMatrix3x3 physics_rotation(transform[0][0],
                               transform[1][0],
                               transform[2][0],
                               transform[0][1],
                               transform[1][1],
                               transform[1][2],
                               transform[2][0],
                               transform[2][1],
                               transform[2][0]);
  btVector3 physics_translation(transform[3][0], transform[3][1], transform[3][2]);
  btTransform physics_transform(physics_rotation, physics_translation);

  //TODO: This is just hardcoded atm. we need to solve it by using proper collision shapes
  physics_manager_.AttachObject(btVector3(1.0f, 1.0f, 1.0f), physics_transform);

  auto program = object.GetMaterial().GetProgram();
  //TODO: this should be cached for every program
  mvp_id_ = static_cast<GLuint>(glGetUniformLocation(program, "mvp_transform"));
  if (current_program_ != program) {
    current_program_ = program;
    glUseProgram(current_program_);
  }
}

}

